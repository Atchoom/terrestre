# Terrestre 

Terrestre is a 2D Platform game made with Unity, by Timothée Denoux and Thibault Lextrait.
It's a project as part of the JIN formation at ENSIIE.

The main goal was to make a basic platform game with Unit, but without using the Rigidbody of Unity for collision and movement.

You can test it at : https://debilogora.itch.io/terrestre


