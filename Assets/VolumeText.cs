﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeText : MonoBehaviour
{
    private Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    public void SetValue(Slider slider)
    {
        if (slider != null)
        {
            text.text = ((int)((slider.value  - slider.minValue) / (slider.maxValue - slider.minValue) * 100)).ToString();
        }
    }
}
