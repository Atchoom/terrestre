﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] Slider slider;
    [SerializeField] PlayerController player;
    public AudioMixer mixer;

    void Start()
    {
        if (PlayerPrefs.HasKey("volume"))
            slider.value = PlayerPrefs.GetFloat("volume", 0);
        else
            Debug.Log("no key");
    }

    public void Back()
    {
        Time.timeScale = 1f;
    }

    public void SetVolume(Slider slider)
    {
        mixer.SetFloat("volume", slider.value);
    }

    public void SetDash(Slider slider)
    {
        if (slider != null) { 
          GameManager.instance.dashSpeed = slider.value;
          if (player != null)
              player.dashSpeed = slider.value;
        }
    }

    public void SetGravity(Slider slider)
    {
        if (slider != null)
        {
            GameManager.instance.gravity = slider.value;
            if (player != null)
                player.BaseGravityForce = slider.value;
        }
    }

    public void SetSpeed(Slider slider)
    {
        if (slider != null)
        {
            GameManager.instance.playerSpeed = slider.value;
            if (player != null)
                player.playerSpeed = slider.value;
        }
    }

    public void SetJump(Slider slider)
    {
        if (slider != null)
        {
            GameManager.instance.jumpForce = slider.value;
            if (player != null)
                player.JumpForce = slider.value;
        }
    }

    public void Save()
    {
        if (slider != null)
        {
            PlayerPrefs.SetFloat("volume", slider.value);
        }
        /*PlayerPrefs.SetFloat("speed", GameManager.instance.playerSpeed);
        PlayerPrefs.SetFloat("gravity", GameManager.instance.gravity);
        PlayerPrefs.SetFloat("dashSpeed", GameManager.instance.dashSpeed);
        PlayerPrefs.SetFloat("jumpForce", GameManager.instance.jumpForce);*/
    }
}
