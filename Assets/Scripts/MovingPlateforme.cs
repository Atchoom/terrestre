﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlateforme : MonoBehaviour
{
    private int nombrePointsMouvements = 0;
    [SerializeField]
    private GameObject[] pointsMouvements;
    [SerializeField]
    private float resetTime;
    [SerializeField]
    private Vector2 nextPosition;
    [SerializeField]
    private float speed;

    private float smooth;
    private int currentPoint = 0;
    private bool sensCroissant = true;
    private int nextPoint = 0;

    private Vector3 direction;

    private void Awake()
    {
        nombrePointsMouvements = pointsMouvements.Length;

    }

    private void Update()
    {
        if (currentPoint == nombrePointsMouvements - 1)
        {
            sensCroissant = false;
        }
        else if (currentPoint == 0)
        {
            sensCroissant = true;
        }
        if (sensCroissant)
        {
            nextPoint = currentPoint +1;
        }
        else
        {
            nextPoint = currentPoint - 1;
        }
        if (Mathf.Abs(transform.position.x - pointsMouvements[currentPoint].transform.position.x) < 0.01f && Mathf.Abs(transform.position.y - pointsMouvements[currentPoint].transform.position.y) < 0.01f)
        {
            currentPoint = nextPoint;
        }
        transform.position = Vector3.MoveTowards(transform.position, pointsMouvements[currentPoint].transform.position, smooth * Time.deltaTime);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
            collision.transform.parent = gameObject.transform;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        collision.transform.parent = null;
    }
}

