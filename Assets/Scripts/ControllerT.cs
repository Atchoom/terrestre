﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerT : MonoBehaviour
{
    [HideInInspector] public float speed;
    [HideInInspector] public float jump;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        speed = Input.GetAxis("Horizontal");
        jump = Input.GetAxis("Jump");
    }
}
