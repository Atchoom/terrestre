﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour
{
    public float bumperSpeed = 1f;
    public Vector2 bumperDirection = new Vector2(0, 0);
}
