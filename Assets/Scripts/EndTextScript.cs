﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTextScript : MonoBehaviour
{
    public GameObject exit;

    // Update is called once per frame
    void Update()
    {
        if(exit.GetComponent<SpriteRenderer>().enabled == true)
        {
            this.GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
