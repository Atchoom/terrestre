﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Infos")]
    public float gravityForce = 1f;
    public float BaseGravityForce = 1f;
    public float playerSpeed = 2f;
    private bool isGrounded = false;
    private bool isWalled = false;
    private bool firstJump = true;
    public float JumpForce = 10f;
    private float deltafact = 0.03f;
    private Vector2 speed;
    private Vector2 nextDirection;
    public GameObject spawnPoint;
    public GameObject options;

    [Header("Debug")]
    private bool HorizontalCollision = false;
    private bool VerticalCollision = false;
    private bool isWallJumping = false;
    public float durationOfJumpPressing = 0.5f;
    private float timeSinceJump;
    private bool isMovingUpward = false;
    private float jumpThreshold;

    private Vector2 nextPosition;
    private LayerMask platformMask;
    private float vertical;
    private float horizontal;
    private bool jumping;
    private ContactFilter2D filter;
    private Vector2 lastWallNormal;
    private float timeWallJump = 0f;
    private bool letsRespawn = false;
    /// <summary>
    /// Particle system
    /// </summary>
    [SerializeField] private ParticleSystem ps;
    [SerializeField] private ParticleSystem deathPS;




    [Header("WallJump")]
    [SerializeField]private float timeWJLimit = 0.7f;
    private Vector2 WJNormal;

    [Header("Dash")]
    private bool isDashing = false;
    public float dashSpeed = 20f;
    [SerializeField] private float dashTimeLimit = 0.5f;
    private bool canDash = true;
    private bool dash = false;
    private float dashDirection;
    private float dashTime = 0f;



    private void Awake()
    {
        platformMask = LayerMask.GetMask("Plateforme");
        filter.SetLayerMask(platformMask);
        filter.useLayerMask = true;
        filter.useTriggers = true;
        speed = new Vector2(0, 0);

        JumpForce = GameManager.instance.jumpForce;
        playerSpeed = GameManager.instance.playerSpeed;
        dashSpeed = GameManager.instance.dashSpeed;
        BaseGravityForce = GameManager.instance.gravity;
    }

    private void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        jumping = Input.GetButtonDown("Jump");
        letsRespawn = Input.GetButtonDown("Cancel");
        dash = Input.GetButtonDown("Fire3");

        if (Input.GetAxis("Cancel") > 0f)
        {
            //Time.timeScale = 0f;
            options.SetActive(true);
        }

        if (letsRespawn)
            return;

        nextPosition = new Vector2(transform.position.x, transform.position.y);
        if (isGrounded)
        {
            speed.y = -gravityForce * Time.deltaTime;
        }
        else {
            speed.y = speed.y - gravityForce * Time.deltaTime;
        }
        if (isGrounded)
            speed.x = horizontal * playerSpeed;
        else
            speed.x = horizontal * playerSpeed * 0.8f;
        if (isWallJumping)
        {
            firstJump = false;
            timeWallJump += Time.deltaTime;
            if (timeWallJump >= timeWJLimit)
            {
                isWallJumping = false;
                timeWallJump = 0f;
            }
            else
            {
                speed.x = (1 - (timeWallJump / timeWJLimit)) * WJNormal.x * playerSpeed * 2f + (timeWallJump / timeWJLimit) * horizontal * playerSpeed * 0.8f;
            }
        }
        if (jumping)
        {
            Jump();

        }
        if (Input.GetAxis("Jump") > float.Epsilon && timeSinceJump < durationOfJumpPressing && isMovingUpward && !isWallJumping && firstJump)
        {
            gravityForce = 0f;
            timeSinceJump += Time.deltaTime;
        }
        else {
            isMovingUpward = false;
            gravityForce = BaseGravityForce;
        }
        if (canDash && dash && (horizontal != 0))
        {
            isDashing = true;
            canDash = false;
            dashTime = 0f;
            dashDirection = horizontal > 0 ? 1 : -1;
        }
            
        if(isDashing)
        {
            dashTime += Time.deltaTime;
            if (dashTime < dashTimeLimit)
            {
                speed.y = 0f;
                speed.x = dashDirection * dashSpeed;
            } else
            {
                isDashing = false;
            }

        }
        nextPosition.x += speed.x * Time.deltaTime;
        nextPosition.y += speed.y * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (letsRespawn)
        {
            Respawn();
            return;
        }
        if (CheckVerticalCollision())
        {
        }
        if (CheckHorizontalCollision())
        {
        }
        transform.position = nextPosition;
    }

    private bool CheckVerticalCollision()
    {
        if(speed.y == 0)
        {
            nextPosition.y = transform.position.y;
            return false;
        }
        RaycastHit2D[] hitGround = new RaycastHit2D[16];
        Collider2D collider = GetComponent<Collider2D>();
        nextDirection = nextPosition - (Vector2)transform.position;
        int nombreCollision = collider.Cast(nextDirection.normalized, filter, hitGround,nextDirection.magnitude);
        if(nombreCollision > 0)
        {
            for(int i = 0; i < nombreCollision; i++)
            {
                VerticalCollision = true;
                if (speed.y > 0) //Vers le haut
                {
                    if (hitGround[i].collider.gameObject.CompareTag("OneWay"))//OneWay platform collision
                    {
                        isGrounded = false;
                        return false;
                    }
                    if (hitGround[i].collider.gameObject.CompareTag("Bumper"))//Bumper platform collision
                    {
                        Bumping(hitGround[i].collider.gameObject.GetComponent<Bumper>());
                        return false;
                    }
                    if (Vector2.Dot(hitGround[i].normal, Vector2.up) < 0)
                    {
                        isGrounded = false;
                        speed.y = 0;
                        nextPosition.y = hitGround[i].centroid.y;
                    }
                }
                else // Vers le bas
                {
                    if (Vector2.Dot(hitGround[i].normal, Vector2.down) < 0)
                    {
                        if (hitGround[i].collider.gameObject.CompareTag("OneWay"))//OneWay platform collision
                        {
                            isGrounded = false;
                            if (Input.GetAxis("Vertical") < 0f && Input.GetAxis("Jump") > 0f) return false;
                            if((transform.position.y - transform.lossyScale.y / 2f <= hitGround[i].collider.transform.position.y + hitGround[i].collider.transform.lossyScale.y / 2f - deltafact))
                            {
                                return false;
                            }
                        }
                        if (hitGround[i].collider.gameObject.CompareTag("Bumper"))//Bumper platform collision
                        {
                            Bumping(hitGround[i].collider.gameObject.GetComponent<Bumper>());
                            return false;
                        }
                        isGrounded = true;
                        canDash = true;
                        firstJump = true;
                        nextPosition.y = hitGround[i].centroid.y;
                    }
                }
            }
            return true;
        }
        isGrounded = false;
        VerticalCollision = false;
        return false;
    }

    private bool CheckHorizontalCollision()
    {
        if (speed.x == 0)
        {
            nextPosition.x = transform.position.x;
            return false;
        }
        RaycastHit2D[] hitGround = new RaycastHit2D[16];
        Collider2D collider = GetComponent<Collider2D>();
        nextDirection = nextPosition - (Vector2)transform.position;
        int nombreCollision = collider.Cast(nextDirection.normalized, filter, hitGround, nextDirection.magnitude);
        if (nombreCollision > 0)
        {
            HorizontalCollision = true;
            /*if (isWallJumping && (timeWallJump > timeWJLimit / 5f))
            {
                isWallJumping = false;
                timeWallJump = 0f;
            }*/
                
            for(int i = 0; i < nombreCollision; i++)
            {
                if (hitGround[i].collider.gameObject.CompareTag("OneWay")) //OneWay platform collision
                {
                    isWalled = false;
                    return false;
                }
                if (hitGround[i].collider.gameObject.CompareTag("Bumper"))//Bumper platform collision
                {
                    Bumping(hitGround[i].collider.gameObject.GetComponent<Bumper>());
                    return false;
                }
                if (speed.x > 0)
                {
                    if (Vector2.Dot(hitGround[i].normal, Vector2.right) < 0)
                    {
                        isWalled = true;
                        speed.x = 0;
                        nextPosition.x = hitGround[i].centroid.x;
                        lastWallNormal = hitGround[i].normal;
                    } else
                    {
                        isWalled = false;
                    }

                }
                else
                {
                    if (Vector2.Dot(hitGround[i].normal, Vector2.left) < 0)
                    {
                        isWalled = true;
                        speed.x = 0;
                        nextPosition.x = hitGround[i].centroid.x;
                        lastWallNormal = hitGround[i].normal;
                    } else
                    {
                        isWalled = false;
                    }
                }
            }
            return true;
        }
        isWalled = false;
        HorizontalCollision = false;
        return false;
    }

    private void Jump()
    {
        if (Input.GetAxis("Vertical") < -jumpThreshold) return;
        if (isWalled && !isGrounded)
        {
            isWallJumping = true;
            isWalled = false;
            firstJump = false;
            timeSinceJump = 0f;
            isMovingUpward = true;
            WJNormal = lastWallNormal;
            speed.y = JumpForce;
            EmitBurst();
            return;
        }
        if (isGrounded)
        {
            isGrounded = false;
            firstJump = true;
            speed.y = JumpForce;
            timeSinceJump = 0f;
            isMovingUpward = true;
            EmitBurst();
        }
        else
        {
            if (firstJump)
            {
                isGrounded = false;
                firstJump = false;
                speed.y = JumpForce;
                timeSinceJump = 0f;
                isMovingUpward = true;
                EmitBurst();
            }
        }


    }

    private void EmitBurst()
    {
        ps.Emit(1);
    }

    private void Bumping(Bumper bump)
    {
        float bumpForce = bump.bumperSpeed;
        Vector2 bumpDir = bump.bumperDirection;
        speed = bumpDir * bumpForce;
        isGrounded = false;
        firstJump = true;
        isMovingUpward = false;
    }

    public void Respawn()
    {
        if(spawnPoint == null)
        {
            Debug.Log("No spawnPoint");
            return;
        }
        deathPS.Emit(30);
        transform.position = spawnPoint.transform.position;
        speed = Vector2.zero;
    }

}



