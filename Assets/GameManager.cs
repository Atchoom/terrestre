﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public float gravity, playerSpeed, dashSpeed, jumpForce;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

        gravity = PlayerPrefs.GetFloat("gravity", 60f);
        playerSpeed = PlayerPrefs.GetFloat("speed", 15f);
        dashSpeed = PlayerPrefs.GetFloat("dashSpeed", 60f);
        jumpForce = PlayerPrefs.GetFloat("jumpForce", 20f);
    }
}
